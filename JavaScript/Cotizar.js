//Usuario y contraseña por defecto
var user = 'Administrador';
var pass = '2019';

//Variables guardadas de registrarse
var NuevoUsuario = "";
var NuevoPassU = "";

//Nav Iniciar sesión del encabezado
var UsuarioI = document.querySelector('.UsuarioIniciado');
var cs = document.getElementsByClassName('CerrarSesion');
var selec = document.getElementsByClassName('seleccion');
var EditC = document.getElementsByClassName('EditarC');
var GU = document.getElementsByClassName('GestionarU');
var btnGuardar = document.querySelector('.BotonGuardar');

function navegar(value){
    //debugger;
    if (value != "" ) {
       if (value == "CerrarSesion") { 
          CerrarSesion();
       } else if (value == "Editar contenido") {
          HabilitarEdicion();
       } else {
          location.href = value;
       }
    }
}

function CambiarUsuario(){
    //debugger;
    //Asignar a las variables los valores locales ingresados en login
    var nombreU = localStorage.getItem("nombreU");
    var passU = localStorage.getItem("passU");
    
    //Variables del nuevo usuario
    NuevoUsuario = localStorage.getItem("NuevoUsuario");
    NuevoPassU = localStorage.getItem("NuevoPassU"); 
 
    //Validación de usuarios existentes
    if(user == nombreU && pass == passU){
       UsuarioI.textContent = nombreU; //Asignar el nombre del usuario en el select
       UsuarioI.disabled = true; //Deshabilitar el boton para no ingresar usuario cuando ya esté iniciado
       cs[0].style.display = 'contents';
       GU[0].style.display = 'contents';
       selec[0].textContent = UsuarioI.text;
       HabilitarEdicion();
    }else if(NuevoUsuario !== null && NuevoPassU !== null){
       UsuarioI.textContent = NuevoUsuario; //Asignar el nombre del usuario en el select
       UsuarioI.disabled = true; //Deshabilitar el boton para no ingresar usuario cuando ya esté iniciado
       cs[0].style.display = 'contents';
       selec[0].textContent = UsuarioI.text;
    }
}

function CerrarSesion(){
    //debugger;
    if(UsuarioI.text == user){
       //Quitar usuario y contraseña 
       localStorage.removeItem('nombreU');
       localStorage.removeItem('passU');
    }else if(UsuarioI.text == NuevoUsuario){
       localStorage.removeItem('NuevoUsuario');
       localStorage.removeItem('NuevoPassU');
    }
    //Actualizar para efectuar los cambios
    location.reload(true);
} 
 
function HabilitarEdicion(){
    
   EditC[0].style.display = "contents";
   EditC[0].addEventListener("click", function(){
      var modificar = confirm("Ha iniciado como Administrador \n \n¿Desea realizar algún cambio en el documento?");
      if(modificar == true){
         EditC[0].style.display = "none";
         btnGuardar.style.display = 'inline';
         HacerCambios();
      } 
   });
}

function HacerCambios(){
    //debugger;
    //Mostrar etiquetas por colores
 
    // Asignar variables para cada etiqueta
    var h1 = document.getElementsByTagName("h1");
    var h2 = document.getElementsByTagName("h2");
    var label = document.getElementsByTagName("label");
    var h4 = document.getElementsByTagName("h4");
    var h5 = document.getElementsByTagName("h5");
 
    for(i=0;i<h1.length;i++){
       h1[i].classList.add("color");
       h1[i].contentEditable = 'true';
    } 
 
    for(i=0;i<h2.length;i++){
        h2[i].classList.add("color");
        h2[i].contentEditable = 'true';
    } 
  
    for(i=0;i<label.length;i++){
        label[i].classList.add("color");
        label[i].contentEditable = 'true';
        label[i].designMode = 'on';
    }
  
    for(i=0;i<h4.length;i++){
       h4[i].classList.add("color");
       h4[i].contentEditable = 'true';
    }
 
    for(i=0;i<h5.length;i++){
       h5[i].classList.add("color");
       h5[i].contentEditable = 'true';
    }
 }

 /* Validación de las cotizaciones */

var MarcaPC = document.getElementById('MarcaPC'),
   ModeloPC = document.getElementById('ModeloPC'),
   MarcaM = document.getElementById('MarcaM'),
   ModeloM = document.getElementById('ModeloM'),
   Software = document.getElementById('Software'),
   Mantenimiento = document.getElementsByClassName('mantenimiento'),
   SO = document.getElementById('SO'),
   Enviar = document.getElementById('Enviar');

function deshabilitarT(){
   MarcaPC.style.display = "none";
   ModeloPC.style.display = "none";
   MarcaM.style.display = "none";
   ModeloM.style.display = "none";
   Software.style.display = "none";
   for(i=0;i<4;i++){
      Mantenimiento[i].style.display = "none";
   }
   SO.style.display = "none";
   Enviar.disabled = true;
}

function instalarSoft(){
   MarcaPC.style.display = "block";
   ModeloPC.style.display = "block";
   MarcaM.style.display = "none";
   ModeloM.style.display = "none";
   Software.style.display = "block";
   for(i=0;i<4;i++){
      Mantenimiento[i].style.display = "none";
   }
   SO.style.display = "none";
   Enviar.disabled = false;
}

function Mante(){
   MarcaPC.style.display = "block";
   ModeloPC.style.display = "block";
   MarcaM.style.display = "none";
   ModeloM.style.display = "none";
   Software.style.display = "none";
   for(i=0;i<4;i++){
      Mantenimiento[i].style.display = "inline";
   }
   SO.style.display = "none";
   Enviar.disabled = false;
}

function format(){
   MarcaPC.style.display = "block";
   ModeloPC.style.display = "block";
   MarcaM.style.display = "none";
   ModeloM.style.display = "none";
   Software.style.display = "none";
   for(i=0;i<4;i++){
      Mantenimiento[i].style.display = "none";
   }
   SO.style.display = "block";
   Enviar.disabled = true;
}

function validarSO(value){
   if(value != ""){
      Enviar.disabled = false;
   }
}

function Limpieza(){
   MarcaPC.style.display = "block";
   ModeloPC.style.display = "block";
   MarcaM.style.display = "none";
   ModeloM.style.display = "none";
   Software.style.display = "none";
   for(i=0;i<4;i++){
      Mantenimiento[i].style.display = "none";
   }
   SO.style.display = "none";
   Enviar.disabled = false;
}

function validarMovil(value){
   if(value == "Red" || value == "Flasheo" || value == "Desbloqueo"){
      MarcaPC.style.display = "none";
      ModeloPC.style.display = "none";
      MarcaM.style.display = "block";
      ModeloM.style.display = "block";
      Software.style.display = "none";
      for(i=0;i<4;i++){
         Mantenimiento[i].style.display = "none";
      }
      SO.style.display = "none";
      Enviar.disabled = false;
   }
}

function cargarFunciones(){
   deshabilitarT();
   CambiarUsuario();
}