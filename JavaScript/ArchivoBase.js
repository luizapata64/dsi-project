debugger;
//Usuario y contraseña por defecto
var user = 'Administrador';
var pass = '2019';

// Nav Iniciar sesión del encabezado
var UsuarioI = document.querySelector('.UsuarioIniciado');
var cs = document.getElementsByClassName('CerrarSesion');
var selec = document.getElementsByClassName('seleccion');
var EditC = document.getElementsByClassName('EditarC');
var GuardC = document.getElementsByClassName('GuardarC');

//Obtener los datos de Registrar
var UsuarioR;
var PassR;
var repetir;
var correo;

//Variables guardadas de registrarse
var NuevoUsuario = "";
var NuevoPassU = "";

//Boton de solicitar servicio
let btn_SolicitarC = document.querySelector('#btn_CServicio');
// let btn_AgregarS = document.querySelector('#btn_AServicio');

//Variables para editar contenido
// var n = 1;

function navegar(value){
   debugger;
   if (value != "" ) {
      if (value == "CerrarSesion") { 
         CerrarSesion();
      } else if (value == "Editar contenido") {
         HabilitarEdicion();
      } else if (value == "Guargar cambios"){
         HabilitarEdicion()
      } else {
         location.href = value;
      }
   }
}

function IniciarSesion(){
   debugger;
   //Usuario y contraseña del Login
   var nombreUsuario = document.getElementById("username").value;
   var passUsuario = document.getElementById("password").value;

   //Usuario y contraseña de un nuevo usuario
   NuevoUsuario = localStorage.getItem("NuevoUsuario");
   NuevoPassU = localStorage.getItem("NuevoPassU"); 

   //debugger;
   //Validación del usuario y contraseña ingresado con los existentes
   if(user == nombreUsuario && pass == passUsuario){
      localStorage.setItem("nombreU",nombreUsuario);
      localStorage.setItem("passU",passUsuario);
      window.location.href='Inicio.html';
   }else if(NuevoUsuario == nombreUsuario && NuevoPassU == passUsuario){
      window.location.href='Inicio.html';
   }else{
      alert("Error en los campos ingresados o usuario no existente");
      location.reload(true); //Actualizar
   }
}

function Registrarse(){
   debugger;
   //Obtener los datos de Registrar
   UsuarioR = document.getElementById("NUsuario").value;
   PassR = document.getElementById("NPass").value;
   repetir = document.getElementById("RepetirPass").value;
   correo = document.getElementById("correo").value;

   function validar_email( email ) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

   //Validación con usuario existentes y contraseña repetida
      if(PassR == repetir && UsuarioR !== user && validar_email(correo) == true && PassR.length > 7 && repetir.length > 7
      && UsuarioR.length > 4){
         //Variables de usuario nuevo
         localStorage.setItem("NuevoUsuario",UsuarioR);
         localStorage.setItem("NuevoPassU",PassR);
         window.location.href='ISesion.html';
      }else{
         alert("Verifique si la contraseña es la misma, el correo electrónico es correcto"+
         " o pruebe con ingresar un nombre diferente de usuario");
         location.reload(true);
      }
}

//Validar si hay un usuario iniciado
function CambiarUsuario(){
   debugger;
   //Asignar a las variables los valores locales ingresados en login
   var nombreU = localStorage.getItem("nombreU");
   var passU = localStorage.getItem("passU");
   
   //Variables del nuevo usuario
   NuevoUsuario = localStorage.getItem("NuevoUsuario");
   NuevoPassU = localStorage.getItem("NuevoPassU"); 

   //Validación de usuarios existentes
   if(user == nombreU && pass == passU){
      UsuarioI.textContent = nombreU; //Asignar el nombre del usuario en el select
      UsuarioI.disabled = true; //Deshabilitar el boton para no ingresar usuario cuando ya esté iniciado
      cs[0].style.display = 'contents';
      // EditC[0].style.display = 'inline';
      selec[0].textContent = UsuarioI.text;
      HabilitarEdicion();
      // btn_AgregarS.style.display = 'inline';
   }else if(/*(NuevoUsuario !== "" && NuevoPassU !== "") &&*/ (NuevoUsuario !== null && NuevoPassU !== null)){
      UsuarioI.textContent = NuevoUsuario; //Asignar el nombre del usuario en el select
      UsuarioI.disabled = true; //Deshabilitar el boton para no ingresar usuario cuando ya esté iniciado
      cs[0].style.display = 'contents';
      selec[0].textContent = UsuarioI.text;
      btn_SolicitarC.style.display = 'inline';
   }
}

function CerrarSesion(){
   debugger;
   if(UsuarioI.text == user){
      //Quitar usuario y contraseña 
      localStorage.removeItem('nombreU');
      localStorage.removeItem('passU');
   }else if(UsuarioI.text == NuevoUsuario){
      localStorage.removeItem('NuevoUsuario');
      localStorage.removeItem('NuevoPassU');
   }

   //Actualizar para efectuar los cambios
   location.reload(true);
}

function HabilitarEdicion(){
   
   if(n%2 == 0){
      /* EditC[0].style.display = 'none';
      GuardC[0].style.display = 'inline';
     GuardC[0].addEventListener("click", function(){
         var modificar = confirm("¿Desea guardar los cambios realizados?");
         if(modificar == true){
            localStorage.removeItem('n');
            location.reload(true);
         } 
      });*/
   }else{
      console.log(EditC[0]);
      EditC[0].style.display = 'inline';
      EditC[0].addEventListener("click", function(){
         var modificar = confirm("Ha iniciado como Administrador \n \n¿Desea realizar algún cambio en el documento?");
         if(modificar == true){
            GuardC[0].style.display = 'inline';
            HacerCambios();
            localStorage.setItem("n", n++);
            n = localStorage.getItem("n");
         } 
      }); 
   }
}

function HacerCambios(){
   debugger;
   //Mostrar etiquetas por colores

   // Asignar variables para cada etiqueta
   var h1 = document.getElementsByTagName("h1");
   var h2 = document.getElementsByTagName("h2");
   var h3 = document.getElementsByTagName("h3");
   var h4 = document.getElementsByTagName("h4");
   var h5 = document.getElementsByTagName("h5");
   var p = document.getElementsByTagName("p");
   var label = document.getElementsByTagName("label");
   var listado = document.getElementsByClassName(".listado");

   for(i=0;i<h1.length;i++){
      h1[i].classList.add("color");
      h1[i].contentEditable = 'true';
   } 

   for(i=0;i<h2.length;i++){
      h2[i].classList.add("color");
      h2[i].contentEditable = 'true';
   }

   for(i=0;i<h3.length;i++){
      h3[i].classList.add("color");
      h3[i].contentEditable = 'true';
   }

   for(i=0;i<h4.length;i++){
      h4[i].classList.add("color");
      h4[i].contentEditable = 'true';
   }

   for(i=0;i<h5.length;i++){
      h5[i].classList.add("color");
      h5[i].contentEditable = 'true';
   }

   for(i=0;i<p.length;i++){
      p[i].classList.add("color");
      p[i].contentEditable = 'true';
   } 

   for(i=0;i<label.length;i++){
      label[i].classList.add("color");
      label[i].contentEditable = 'true';
      label[i].designMode = 'on';
   }

   listado.classList.add("color");
   listado.contentEditable = 'true';
}
